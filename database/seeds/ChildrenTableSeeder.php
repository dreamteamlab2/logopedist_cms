<?php

use Illuminate\Database\Seeder;

class ChildrenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Дети Админа (для примера)

        DB::table('children')->insert([
            'name' => "Алиса",
            'birthdate' => "2016-08-09 00:00:00",
            'description_file' => null,
            'parent_id' => \App\User::query()->where('is_admin', '=', true)->pluck('id')[0],
            'group_id' => null,
        ]);

        DB::table('children')->insert([
            'name' => "Саша",
            'birthdate' => "2014-04-14 00:00:00",
            'description_file' => null,
            'parent_id' => \App\User::query()->where('is_admin', '=', true)->pluck('id')[0],
            'group_id' => null,
        ]);

        DB::table('children')->insert([
            'name' => "Вика",
            'birthdate' => "2013-11-12 00:00:00",
            'description_file' => null,
            'parent_id' => \App\User::query()->where('is_admin', '=', true)->pluck('id')[0],
            'group_id' => null,
        ]);


        DB::table('children')->insert([
            'name' => "Миша",
            'birthdate' => "2016-07-22 00:00:00",
            'description_file' => null,
            'parent_id' => \App\User::query()->where('is_admin', '=', true)->pluck('id')[0],
            'group_id' => null,
        ]);

        DB::table('children')->insert([
            'name' => "Руслан",
            'birthdate' => "2015-03-26 00:00:00",
            'description_file' => null,
            'parent_id' => \App\User::query()->where('is_admin', '=', true)->pluck('id')[0],
            'group_id' => null,
        ]);

        DB::table('children')->insert([
            'name' => "Катя",
            'birthdate' => "2013-07-12 00:00:00",
            'description_file' => null,
            'parent_id' => \App\User::query()->where('is_admin', '=', true)->pluck('id')[0],
            'group_id' => null,
        ]);

        DB::table('children')->insert([
            'name' => "Максим",
            'birthdate' => "2017-01-25 00:00:00",
            'description_file' => null,
            'parent_id' => \App\User::query()->where('is_admin', '=', true)->pluck('id')[0],
            'group_id' => null,
        ]);
    }
}
