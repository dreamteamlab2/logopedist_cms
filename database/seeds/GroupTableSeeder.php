<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // тестовая группа
        DB::table('groups')->insert([
            'name' => "Дошколята",
            'size' => 3
        ]);
    }
}
