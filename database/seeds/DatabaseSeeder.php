<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableAdminSeeder::class);
        //$this->call(AchievementsTableSeeder::class);
        //$this->call(DirectionsTableSeeder::class);
        $this->call(ChildrenTableSeeder::class);
        $this->call(GroupTableSeeder::class);
    }
}
