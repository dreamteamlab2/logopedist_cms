<?php

use Illuminate\Database\Seeder;

class AchievementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('achievements')->insert([
            'image' => "gramota.png",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque non diam dictum pulvinar.',
        ]);

        DB::table('achievements')->insert([
            'image' => "diploma.png",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque non diam dictum pulvinar.',
        ]);

        DB::table('achievements')->insert([
            'image' => "certificate.png",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque non diam dictum pulvinar.',
        ]);

        DB::table('achievements')->insert([
            'image' => "certificate2.png",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque non diam dictum pulvinar.',
        ]);

        DB::table('achievements')->insert([
            'image' => "degree.jpg",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque non diam dictum pulvinar.',
        ]);
    }
}
