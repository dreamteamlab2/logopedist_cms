<?php

use Illuminate\Database\Seeder;

class DirectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directions')->insert([
            'image' => "success.png",
            'title' => 'Диагностика речевого развития',
            'description' => 'Диагностические методики по обследованию разных сторон речи.',
        ]);

        DB::table('directions')->insert([
            'image' => "study.png",
            'title' => 'Индивидуальные и групповые занятия',
            'description' => 'Занятия общеразвивающей и предметной направленности.',
        ]);

        DB::table('directions')->insert([
            'image' => "graduation-hat.png",
            'title' => 'Подготовка к школе',
            'description' => 'Формирование предпосылок обучения грамоте, развитие мелкой моторики',
        ]);

        DB::table('directions')->insert([
            'image' => "chat.png",
            'title' => 'Коррекция устной речи',
            'description' => 'Преодоление нарушений звукопроизношения, постановка звуков.',
        ]);

        DB::table('directions')->insert([
            'image' => "puzzle.png",
            'title' => 'Занятия с детьми ОВЗ',
            'description' => 'Коррекция аномального поведения и затруднений в социальном взаимодействии и коммуникациях.',
        ]);

        DB::table('directions')->insert([
            'image' => "musical-note.png",
            'title' => 'Логоритмика',
            'description' => 'Система музыкально-двигательных, музыкально-речевых игр и упражнений.',
        ]);
    }
}
