<?php

use Illuminate\Database\Seeder;

class UsersTableAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Администратор",
            'email' => 'admin@admin.ru',
            'password' => bcrypt('admin111'),
            'phone' => "+7 (777) 777-77-77",
            'is_admin' => true,
        ]);
    }
}
