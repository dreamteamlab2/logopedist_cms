<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Achievement;
use App\Direction;

//пути для администратора
Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth']], function(){
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::get('/dashboard/edit', 'DashboardController@edit')->name('admin.dashboard.edit');
    Route::put('/dashboard/update', 'DashboardController@update')->name('admin.dashboard.update');

    Route::resource('/direction', 'DirectionController', ['as'=>'admin']);
    Route::resource('/news', 'NewsController', ['as'=>'admin']);
    Route::resource('/achievements', 'AchievementController', ['as'=>'admin']);
    Route::resource('/feedback', 'FeedbackController', ['as'=>'admin']);
    Route::resource('/review', 'ReviewController', ['as'=>'admin']);
    Route::resource('/groups', 'GroupController', ['as'=>'admin']);

    Route::resource('/user', 'UserController', ['as' => 'admin']);
    Route::get('/children', 'UserController@getAllChildren', ['as' => 'admin'])->name('admin.children');
});

Route::group(['prefix'=>'auth', 'namespace'=>'Auth', 'middleware'=>['auth']], function(){
    Route::resource('/children', 'ChildController', ['as'=>'auth']);
});

Auth::routes();

// здесь тоже понадобится контроллер, так как будет вытаскидать данные из бд
Route::get('/', function () {
    return view('welcome', [
        'achievements'=> Achievement::all(),
        'directions'=> Direction::all()
    ]);
});

Route::get('/admin/schedule', function () {
    return view('admin.schedule.index');
})->name('admin.schedule');


Route::get('/home', 'HomeController@index')->name('home');


// IndexController возвращает вьюшки для основных страниц
Route::get('/contact', 'IndexController@contact')->name('contact');
Route::get('/news', 'IndexController@news')->name('news');
Route::get('/review', 'IndexController@review')->name('review');
