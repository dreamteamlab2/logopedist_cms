<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    protected $fillable = ['name', 'birthdate', 'parent_id'];

    public function parent()
    {
        // User::find($this->parent_id);
        // $this->belongsTo('App\User', 'parent_id', 'id');
        return $this->belongsTo('App\User', 'parent_id', 'id');
    }
    public $timestamps = false;
    protected $primaryKey = 'child_id';
}
