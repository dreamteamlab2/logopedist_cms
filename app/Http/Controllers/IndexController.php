<?php

namespace App\Http\Controllers;

use App\News;
use App\Review;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function contact()
    {
        return view('contact', [

        ]);
    }

    public function news() {
        return view('news', [
            'news' => News::all()
        ]);
    }

    public function review() {
        return view('review.index', [
          'reviews' => Review::all(),
        ])->with('published',true);
    }
}
