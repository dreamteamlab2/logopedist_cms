<?php

namespace App\Http\Controllers\Admin;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //вернем список
        return view('admin.news.index', [
            // описываем в массиве, какие параметры будем возвращать
            'news' => News::paginate(6)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create', [
            'news_item'=> new News(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump(Input::file('image')); die;

        $file = Input::file('image');

        $image = Image::make(Input::file('image'));
        $path = storage_path().'/app/public/news/';
        $time = time();

        $image->save($path.$time.'.'.$file->getClientOriginalName());
        $image->resize(50, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $image->save($path.'previews/'.$time.'.'.$file->getClientOriginalName());

        $achievement = News::create($request->all());
        $achievement->image = $time.'.'.$file->getClientOriginalName();
        $achievement->save();

        return redirect()->route('admin.news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
//        return view('admin.news.edit', [
//            'news_item'   => $news,
//            'news' => News::all()
//        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view('admin.news.edit', [
            'news_item'   => $news,
         //   'news' => News::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $file = Input::file('image');
        if ($file) {
            $image = Image::make(Input::file('image'));
            $path = storage_path().'/app/public/news/';
            $time = time();

            $image->save($path.$time.'.'.$file->getClientOriginalName());
            $image->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($path.'previews/'.$time.'.'.$file->getClientOriginalName());
        }

        $news->update($request->all());
        if ($file) {
            $news->image = $time.'.'.$file->getClientOriginalName();
        }

        $news->save();

        return redirect()->route('admin.news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return redirect()->route('admin.news.index');
    }
}
