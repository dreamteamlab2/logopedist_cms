<?php

namespace App\Http\Controllers\Admin;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.review.index', [
            // описываем в массиве, какие параметры будем возвращать
            'reviews' => Review::paginate(10)
        ]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        Review::create($request->all());
        return view('review.success');
    }

    public function show(Review $review)
    {

    }

    public function edit(Review $review)
    {
        //
    }

    public function update(Request $request, Review $review)
    {
        $review->published = true;
        $review->save();
        return redirect()->route('admin.review.index');
    }

    public function destroy(Review $review)
    {
        $review->delete();
        return redirect()->route('admin.review.index');
    }
}
