<?php

namespace App\Http\Controllers\Admin;

use App\Child;
use App\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.groups.index', [
            'groups' => Group::orderBy('id', 'desc')->paginate(5),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.groups.create', [
            'group' => new Group(),
            'available' => Child::where('group_id', '=', null)->get(),
            'students' => null,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // массив с id выбранных детей
        $selectedChildren = $request->input('selected');
        // создаем пустой объект группы
        $group = Group::create([
            'name' => $request->input('group-name'),
            'size' => count($selectedChildren)]);

        $group->save();

        $currChild = null;
        // выставляем зависимости между детьми и группами
        foreach ($selectedChildren as $childId) {
            $currChild = Child::find($childId);
            $currChild->group_id = $group->id;
            $currChild->save();
        }

        return redirect()->route('admin.groups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        $selected_group_id = $group->id;
        $students = Child::query()->where('group_id', '=', $selected_group_id);

        return view('admin.groups.group-profile', [
            'students' => $students->paginate(10),
            'group' => $group,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('admin.groups.edit', [
            'group' => $group,
            'students' => Child::where('group_id', '=', $group->id)->get(),
            'available' => Child::where('group_id', '=', null)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        // массив с id детей из нового списка
        $selectedChildren = $request->input('selected');
        // массив с id детей из старого списка
        $prevSelectedChildren = Child::where('group_id', '=', $group->id)->pluck('child_id')->toArray();
        // удаленные из списка дети
        $deleted = array_diff($prevSelectedChildren, $selectedChildren);
        // добавленные в список дети
        $added = array_diff($selectedChildren, $prevSelectedChildren);

        // изменяем объект группы
        $group->name = $request->input('group-name');
        $group->size = count($selectedChildren);

        // сохраняем объект группы
        $group->save();

        $currChild = null;
        // удаляем зависимость между удаленными детьми и группой
        foreach ($deleted as $childId) {
            $currChild = Child::find($childId);
            $currChild->group_id = null;
            $currChild->save();
        }

        $currChild = null;
        // добавляем зависимость между добавленными детьми и группой
        foreach ($added as $childId) {
            $currChild = Child::find($childId);
            $currChild->group_id = $group->id;
            $currChild->save();
        }

        return redirect()->route('admin.groups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $group->delete();
        return redirect()->route('admin.groups.index');
    }
}
