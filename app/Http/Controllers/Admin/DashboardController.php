<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Info;
//use App\User;

class DashboardController extends Controller
{
    // домашняя страница для администратора

    public function dashboard() {
        return view('admin.dashboard', [
            'info' => Info::paginate(1)
        ]);
    }

    public function edit(Info $info)
    {
        return view('admin.info.edit', [
            'info_item'   => $info,
        ]);
    }

//    public function store(Request $request)
//    {
//        Info::create($request->all());
//        return redirect()->route('admin.dashboard');
//    }

    public function update(Request $request, Info $info)
    {
//        var_dump($info);die;
        $info->update($request->except(''));
        return redirect()->route('admin.index');
    }
}
