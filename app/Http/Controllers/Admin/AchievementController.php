<?php

namespace App\Http\Controllers\Admin;

use App\Achievement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;


class AchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.achievements.index', [
            'achievements' => Achievement::orderBy('id', 'desc')->paginate(5),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.achievements.create', [
            'achievement'=> new Achievement()
//            'directions' => Direction::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = Input::file('image');

        if ($file) {
            $image = Image::make(Input::file('image'));
            $path = storage_path().'/app/public/achievements/';
            $time = time();

            $image->save($path.$time.'.'.$file->getClientOriginalName());
            $image->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($path.'previews/'.$time.'.'.$file->getClientOriginalName());
        }

        $achievement = Achievement::create($request->all());

        if ($file) {
            $achievement->image = $time.'.'.$file->getClientOriginalName();
        }

        $achievement->save();

        return redirect()->route('admin.achievements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Achievement  $achievments
     * @return \Illuminate\Http\Response
     */
    public function show(Achievement $achievments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function edit(Achievement $achievement)
    {
        return view('admin.achievements.edit', [
            'achievement' => $achievement
//            'directions' => Direction::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Achievement  $achievment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Achievement $achievement)
    {
        $file = Input::file('image');
        if ($file) {
            $image = Image::make(Input::file('image'));
            $path = storage_path().'/app/public/achievements/';
            $time = time();

            $image->save($path.$time.'.'.$file->getClientOriginalName());
            $image->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($path.'previews/'.$time.'.'.$file->getClientOriginalName());
        }

        $achievement->update($request->all());
        if ($file) {
            $achievement->image = $time.'.'.$file->getClientOriginalName();
        }

        $achievement->save();


        return redirect()->route('admin.achievements.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Achievement $achievement)
    {
        $achievement->delete();
        return redirect()->route('admin.achievements.index');
    }
}
