<?php

namespace App\Http\Controllers\Admin;

use App\Direction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class DirectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //вернем список
        return view('admin.direction.index', [
            // описываем в массиве, какие параметры будем возвращать
            'directions' => Direction::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.direction.create', [
            'direction'=> new Direction(),
            'directions' => Direction::all() ///
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = Input::file('image');

        if ($file) {
            $image = Image::make(Input::file('image'));
            $path = storage_path().'/app/public/directions/';
            $time = time();

            $image->save($path.$time.'.'.$file->getClientOriginalName());
            $image->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($path.'previews/'.$time.'.'.$file->getClientOriginalName());
        }


        $direction = Direction::create($request->all());

        if ($file) {
            $direction->image = $time.'.'.$file->getClientOriginalName();
        }

        $direction->save();

        return redirect()->route('admin.direction.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Direction  $direction
     * @return \Illuminate\Http\Response
     */
    public function show(Direction $direction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Direction  $direction
     * @return \Illuminate\Http\Response
     */
    public function edit(Direction $direction)
    {
        return view('admin.direction.edit', [
            'direction'   => $direction,
            'directions' => Direction::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Direction  $direction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Direction $direction)
    {
        $file = Input::file('image');
        if ($file) {
            $image = Image::make(Input::file('image'));
            $path = storage_path().'/app/public/directions/';
            $time = time();

            $image->save($path.$time.'.'.$file->getClientOriginalName());
            $image->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($path.'previews/'.$time.'.'.$file->getClientOriginalName());
        }

        $direction->update($request->all());
        if ($file) {
            $direction->image = $time.'.'.$file->getClientOriginalName();
        }

        $direction->save();

        return redirect()->route('admin.direction.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Direction  $direction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Direction $direction)
    {
        $direction->delete();
        return redirect()->route('admin.direction.index');
    }

    public function imageLoad(Request $request){

    }
}
