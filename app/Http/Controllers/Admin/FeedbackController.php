<?php

namespace App\Http\Controllers\Admin;

use App\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class FeedbackController extends Controller
{

    public function index()
    {
        return view('admin.feedback.index', [
            'feedbacks' => Feedback::paginate(10)
        ]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
//        $sender = $request->get('sender');
//        $message = $request->get('message');

        // отправка письма на почту логопеду
        Mail::send(['text'=>'admin.mail.mail_form'], ['name', 'Message for logopedist'], function($message){
            $message->to('logopedtomsk@yandex.ru', 'Message to logopedist')->subject('Новое письмо с сайта');
            $message->from('logopedtomsk@yandex.ru','Сайт логопеда');
        });

        Feedback::create($request->all());
        return view('admin.feedback.success');
    }

    public function show(Feedback $feedback)
    {
        return view('admin.feedback.feedback_item', [
            'feedback'   => $feedback
        ]);
    }

    public function edit(Feedback $feedback)
    {
        //
    }

    public function update(Request $request, Feedback $feedback)
    {
        //
    }

    public function destroy(Feedback $feedback)
    {
        $feedback->delete();
        return redirect()->route('admin.feedback.index');
    }
}
