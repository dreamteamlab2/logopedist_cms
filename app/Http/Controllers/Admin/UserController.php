<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Child;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
//        возвращаем список детей
        return view('admin.user.index', [
            'users' => User::paginate(10),
        ]);
    }

    public function getAllChildren() {
        return view('admin.children.index', [
            // описываем в массиве, какие параметры будем возвращать
            'children' => Child::paginate(10),
        ]);
    }

//    public function profile(User $user){
//        return view('admin.user.user_profile', [
//            'user' => $user
//        ]);
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        User::create($request->all());
//        return redirect()->route('admin.user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.user.user_profile', [
            'user'   => $user
        ]);
    }

//    public function showFromChildrenList(Child $child)
//    {
//        return view('admin.user.user_profile', [
//            'user'   => $child->parent(),
//        ]);
//    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
