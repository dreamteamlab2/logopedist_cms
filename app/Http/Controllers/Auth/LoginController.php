<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
// use App\Http\Controllers\Auth\FixedAuthenticatesUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    //use FixedAuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectPath($is_admin) {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return ($is_admin) ? '/admin' : '/home';

        //return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

    public function sendLoginResponse(Request $request){
        $userId = $this->guard()->user()->getAuthIdentifier();
        $isUserAdmin = User::query()->where('id', '=', $userId)->get()[0]->__get('is_admin');

        //your implementation
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath($isUserAdmin));
    }
}
