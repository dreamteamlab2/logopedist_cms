<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['sender', 'message', 'phone', 'email', 'type'];
}
