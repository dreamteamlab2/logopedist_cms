<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['author_id', 'text'];

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id', 'id');
    }
}
