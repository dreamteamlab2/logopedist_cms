<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['title', 'short_description', 'description', 'image'];

    public function scopeGetOrdered($query)
    {
        return $query->orderBy('created_at', 'desc')->get();
    }
}
