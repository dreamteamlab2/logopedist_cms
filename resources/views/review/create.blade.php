<form action="{{route('admin.review.store')}}" method="post"
      onsubmit="if(confirm('Отправить отзыв?'))
           { return true }
           else { return false}" class="question-form shadow_form">

    <input type="hidden" name="author_id" value="{{Auth::user()->id ?? ''}}">
    {{ csrf_field() }}
    <div class="title">Оставьте свой отзыв</div>
    <div class="form-group">
        <textarea class="form-control" required name="text" cols="10" rows="7" placeholder="Ваш отзыв"></textarea>
    </div>
    <div class="checkbox">
        <label><input type="checkbox"> Я ознакомлен(а) с правилами конфиденциальности</label>
    </div>
    <button type="submit" class="ordinary-button orange">Отправить отзыв </button>
</form>
