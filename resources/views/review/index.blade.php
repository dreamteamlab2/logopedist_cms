@extends('layouts.app')

@section('content')
    @include('layouts/header')

    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 col-12">
                @forelse($reviews as $review)
                    <div class="one-review">
                        <div class="header">
                            <img src="{{ asset('img/avatar.png') }}" class="img-responsive">
                            <div class="post_autor"><h5>{{$review->author->name}}</h5></div>
                            <div class="post_data"> {{date('d-m-Y',strtotime($review->created_at))}} </div>
                        </div>
                        <p>{{$review->text}}</p>
                        {{--<a class="read_more"> Читать дальше </a>--}}
                    </div>
                @empty
                    <h3>Отзывов пока нет, но вы держитесь</h3>
                @endforelse
            </div>

			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-12">
                @include('review/create')
                {{--@include('review/not-login')--}}
			</div>
        </div>
    </div>


    <!--Footer-->
    @include('layouts/footer')
    <!--Модальное окно для обратного звонка-->
    @include('layouts/call-modal')
    <!--Подключение сторонних js-файлов-->
    @include('layouts/js')

@endsection
