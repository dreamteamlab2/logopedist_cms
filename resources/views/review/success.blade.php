@extends('layouts.app')

@section('content')
    @include('layouts/header')

    <div class="container">
        <div class="alert alert-success alert-message d-flex rounded p-0" role="alert">
            <div class="alert-icon d-flex justify-content-center align-items-center flex-grow-0 flex-shrink-0 py-3">
                <i class="fa fa-check"></i>
            </div>
            <div class="d-flex flex-grow-8 align-items-center py-2 pr-1 text">
                Спасибо Вам за обратную связь!
            </div>
        </div>
    </div>

@endsection
