<div class="col-lg-6">
    <div class="form-group row">
        <label class="col-md-3 col-form-label">Имя и Фамилия: </label>
        <input name="name" value="{{$child->name}}" type="text" class="form-control col-md-9" placeholder="Имя ребенка" required>
    </div>
    <div class="form-group row">
        <label class="col-md-3 col-form-label">Дата рождения:</label>
        <input name="birthdate" value="{{date('Y-m-d',strtotime($child->birthdate))}}" type="date" class="form-control col-md-9" required>
    </div>

    <input class="btn btn-primary" type="submit" value="Сохранить">
</div>
