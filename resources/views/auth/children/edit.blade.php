@extends('layouts.app')
@include('layouts/easy-header')

@section('content')
    <div class="container">
        <h4>Редактировать информацию о ребенке</h4>
        <form class="form-horizontal" action="{{route('auth.children.update', $child->child_id)}}" method="post">
            <input type="hidden" name="_method" value="put">
            <input type="hidden" name="parent_id" value="{{Auth::user()->id ?? ''}}">
            {{ csrf_field() }}
            @include('auth.children.form')
        </form>
    </div>
@endsection