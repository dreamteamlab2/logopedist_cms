@extends('layouts.app')
@include('layouts/easy-header')

@section('content')
    <div class="container">
        <h4>Добавить информацию о ребенке</h4>
        <form class="form-horizontal" action="{{route('auth.children.store')}}" method="post">
            <input type="hidden" name="parent_id" value="{{Auth::user()->id ?? ''}}">
            {{ csrf_field() }}
            @include('auth.children.form')
        </form>
    </div>
@endsection