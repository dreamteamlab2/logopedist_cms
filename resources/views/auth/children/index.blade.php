@extends('layouts.app')
@include('layouts/easy-header')

@section('content')
    {{--<style>--}}
        {{--.child_form{--}}
            {{--/*border-bottom: #c8c8c8 1px solid;*/--}}
            {{--/*margin-bottom: 20px;*/--}}
            {{--/*padding: 20px;*/--}}
        {{--}--}}
        {{--.child_form .form-group{--}}
            {{--margin-bottom:0;--}}
        {{--}--}}
    {{--</style>--}}
    <div class="container">
        <h4>Информация о детях</h4>
        @forelse(Auth::user()->children as $child)
            <div class="row">
                <div class="col-lg-6">
                    <form class="child_form">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Имя: </label>
                            <div class="col-md-10 col-form-label">{{$child->name}}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Дата рождения:</label>
                            <div class="col-md-10 col-form-label">{{date('d-m-Y',strtotime($child->birthdate))}}</div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-1">
                    <form onsubmit="if(confirm('Удалить?')){return true} else {return false}"
                          action="{{route('auth.children.destroy', ['child_id'=>$child])}}" method="post">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <a class="btn btn-default" href="{{route('auth.children.edit', $child)}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        <button type="submit" class="btn"><i class="far fa-trash-alt"></i></button>
                    </form>
                </div>
            </div>

        @empty
            <h5>Не внесены данные о детях</h5>
        @endforelse

        <a href="{{route('auth.children.create')}}" class="ordinary-button orange">
            Добавить ребенка<i class="far fa-plus-square"></i></a>
    </div>
@endsection
