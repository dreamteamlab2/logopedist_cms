@extends('layouts.app')

@include('layouts/easy-header')

@section('content')
<div class="container" id="sign-in">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Регистрация') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="parentLastName" class="col-md-4 col-form-label text-md-right">{{ __('Фамилия:') }}</label>

                            <div class="col-md-6">
                                <input id="parentLastName" type="text" class="form-control{{ $errors->has('parentLastName') ? ' is-invalid' : '' }}" name="parentLastName" value="{{ old('parentLastName') }}" required autofocus>

                                @if ($errors->has('parentLastName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('parentLastName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="parentName" class="col-md-4 col-form-label text-md-right">{{ __('Имя:') }}</label>

                            <div class="col-md-6">
                                <input id="parentName" type="text" class="form-control{{ $errors->has('parentName') ? ' is-invalid' : '' }}" name="parentName" value="{{ old('parentName') }}" required autofocus>

                                @if ($errors->has('parentName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('parentName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="parentPatronic" class="col-md-4 col-form-label text-md-right">{{ __('Отчество:') }}</label>

                            <div class="col-md-6">
                                <input id="parentPatronic" type="text" class="form-control{{ $errors->has('parentPatronic') ? ' is-invalid' : '' }}" name="parentPatronic" value="{{ old('parentPatronic') }}" required autofocus>

                                @if ($errors->has('parentPatronic'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('parentPatronic') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Телефон:') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Электронная почта:') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль:') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Подтвердите пароль:') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="ordinary_button orange">
                                    {{ __('Зарегистрироваться') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!--Подключение сторонних js-файлов-->
@include('layouts/js')