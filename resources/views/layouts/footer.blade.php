<footer>
    <div class="contact">
        <div class="line">
            <span id="phone"><i class="fas fa-mobile-alt"></i>+7-903-952-98-18</span>
            <span id="address"><i class="far fa-envelope"></i>t_kajer@mail.ru</span>
        </div>
        <a title="Перейти к карте" href="#"><i class="fas fa-map-marker-alt"></i>Томск, пл. Кирова 58, стр. 55, оф. 69</a>
    </div>
    <div class="links">
        <div>
            <a href="#"><i class="fab fa-vk"></i></a>
            <a href="#"><i class="fab fa-odnoklassniki"></i></a>
            <a href="#"><i class="fab fa-viber"></i></a>
            <a href="#"><i class="fab fa-whatsapp"></i></a>
        </div>
        <span>Следите за мной в моих социальных сетях :)</span>
    </div>
    <div class="brand">
        <img src = "img/logo.png">
    </div>
</footer>