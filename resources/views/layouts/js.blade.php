<!-- Дефолтовый js-файл -->
<script src="{{ asset('js/app.js') }}" defer></script>
<!--JQuery-->
<script src="{{ asset('js/jquery.min.js') }}" defer></script>
<!--Bootstrap-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
{{--<script src="{{ asset('js/bootstrap.min.js') }}" defer></script>--}}
<!--Lightbox-->
<script src="{{ asset('js/lightbox.min.js') }}" defer></script>
<!--Bootstrap-->
<script src="{{ asset('js/slick.min.js') }}" defer></script>
<!--Файл с собственными скриптами-->
<script src="{{ asset('js/main.js') }}" defer></script>


