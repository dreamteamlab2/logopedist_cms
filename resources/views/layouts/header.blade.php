<!--Header-->
<header>
    <div class="brand">
        <a href="{{ url('/') }}"><img src = "{{ asset('img/logo.png') }}"></a>
    </div>
    <nav class="navbar-menu navbar navbar-expand-lg full-navbar">
        <div class="collapse navbar-collapse" id="main_navbar">
            <ul class="navbar-nav">
                <!-- Ссылка -->
                <li class="nav-item {{$_SERVER['REQUEST_URI'] == '/' ? 'active' : ''}}">
                    <a class="nav-link" href="{{ url('/') }}">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/news') }}">Новости</a>
                </li>
                <!-- Выпадающий список -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Полезные материалы</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a href="#">Артикуляционная гимнастика</a></li><li class="divider"></li>
                        <li><a href="#">Дыхательная гимнастика</a></li><li class="divider"></li>
                        <li><a href="#">Мелкая моторика</a></li><li class="divider"></li>
                        <li><a href="#">Рекомендации</a></li><li class="divider"></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/review') }}">Отзывы</a>
                </li>
                <li class="nav-item {{$_SERVER['REQUEST_URI'] == '/contact' ? 'active' : ''}}">
                    <a class="nav-link " href="{{ url('/contact') }}">Контакты</a>
                </li>
                <li class="mobile_menu"> <a href="#">Поиск по сайту<i class="fas fa-search"></i></a></li>
                <li class="mobile_menu" data-toggle="modal" data-target="#callModal" data-whatever="@mdo"><a href="#">Заказать обратный звонок<i class="fas fa-phone"></i></a></li>
                <li class="mobile_menu"> <a href="#">Версия для слабовидящих<i class="fas fa-glasses"></i></a></li>
                <li class="mobile_menu"> <a href="#">Вход<i class="fas fa-sign-in-alt"></i></a></li>
            </ul>
        </div>
    </nav>

    <div id="search_area">
        <input class="search_field" type="text" placeholder="Текст для поиска...">

    </div>
    <div class="icons_area">
        <button id="search_icon" class="btn_icon btn_icon_text"><i class="fas fa-search"></i></button>
        <button id="phone_icon" class="btn_icon btn_icon_text" data-toggle="modal"
                data-target="#callModal" data-whatever="@mdo">
            {{--<a href="{{route('admin.feedback.create')}}">--}}
                <i class="fas fa-phone"></i>
        </button>
        <button id="glasses_icon" class="btn_icon btn_icon_text">
            <i class="fas fa-glasses"></i>
        </button>
        <button id="sign_icon" class="btn_icon btn_icon_text">
            <a href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i></a>
        </button>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_navbar" aria-controls="main_navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
    </button>

</header>
