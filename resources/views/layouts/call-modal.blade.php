<!--Модальное окно для обратного звонка-->
<div class="modal fade" id="callModal" tabindex="-1" role="dialog" aria-labelledby="callModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h5 class="modal-title" id="callModalLabel"> Закажите обратный звонок</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{--Form--}}
            <form action="{{route('admin.feedback.store')}}" method="post">
                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="form-group">
                        <label for="sender" class="col-form-label">Ваше имя:</label> <span class="required_star">*</span>
                        <input name="sender" type="text" value="" class="form-control" id="sender" required>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-form-label">Номер телефона:</label> <span class="required_star">*</span>
                        <input name="phone" type="text" class="form-control" id="phone" required>
                    </div>
                    <div class="form-group">
                        <label for="message" class="col-form-label">Дополнительный комментарий:</label>
                        <textarea name="message" class="form-control" id="message"></textarea>
                    </div>
                    <small><span class="required_star">*</span> поля, обязательные для заполнения</small>
                    <div class="checkbox">
                        <label><input type="checkbox" class="form-check-input" id="invalidCheck"> Я ознакомлен(а) с правилами конфиденциальности</label>
                    </div>
                    <input type="hidden" name="type" value="phone">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Отправить заявку</button>
                </div>
            </form>
        </div>
    </div>
</div>
