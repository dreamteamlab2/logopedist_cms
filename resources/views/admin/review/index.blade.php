<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')
    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')
    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Отзывы @endslot
                @endcomponent
                <h3>Отзывы</h3>

                <div id="reviews-list" class="items-list">
                    <div class="items-list-header">
                        <div>Дата</div>
                        <div>Отправитель</div>
                        <div>Текст отзыва</div>
                        <div>Статус</div>
                        <div>Удалить</div>
                    </div>
                    <div class="items-list-body">
                        @forelse($reviews as $review)
                            <div class="items-list-row">
                                <div>{{date('d-m-Y',strtotime($review->created_at))}}</div>
                                <div>{{$review->author->name}}</div>
                                <div>{{$review->text}}</div>
                                <form class="actions" onsubmit="if(confirm('Вы действительно хотите опубликовать отзыв?'))
                                   { return true } else { return false}"
                                      action="{{route('admin.review.update', $review)}}" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_method" value="put">
                                    {{ csrf_field() }}
                                    @if($review->published == false)
                                        <button type="submit" title="Опубликовать" class="ordinary-button orange">Опубликовать</button>
                                    @else
                                        <h6 class="published-success"><i class="fas fa-check"></i>Опубликован</h6>
                                    @endif
                                </form>
                                <form class="actions" onsubmit="if(confirm('Вы действительно хотите удалить?'))
                                   { return true } else { return false}"
                                      action="{{route('admin.review.destroy', $review)}}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button type="submit" title="Удалить элемент" class="ordinary-button orange"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </div>
                        @empty
                            <div class="items-list-row">
                                <div> :( </div>
                                <div> :( </div>
                                <div class="mid_text">Данные отсутствуют</div>
                                <div> :( </div>
                            </div>
                        @endforelse
                    </div>
                </div>
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        {{$reviews->links()}}
                    </ul>
                </nav>
            </div>
        </div>
    </main>


@endsection
