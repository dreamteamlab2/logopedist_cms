@extends('layouts.app')
@include('admin.layouts.app_admin')

@section('content')
    <main class="l-main">
        <div class="content-wrapper col-lg-8">
            @include('admin.info.index')
            @include('admin.socials.index')
        </div>
    </main>
@endsection

