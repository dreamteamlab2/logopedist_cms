<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Главная</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.groups.index')}}">Список групп</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Создание новой группы</li>
                    </ol>
                </nav>

                <div class="large_text">Создание новой группы</div>
                <form class="form-horizontal" action="{{route('admin.groups.store')}}" method="post"
                      enctype="multipart/form-data">
                    {{--передаем токен в форме через хелпер--}}
                    {{ csrf_field() }}

                    {{--Form Include--}}
                    @include('admin.groups.form')
                </form>
            </div>
        </div>
    </main>

@endsection