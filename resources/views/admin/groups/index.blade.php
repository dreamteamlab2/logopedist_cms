<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Группы @endslot
                @endcomponent

                <h3>Список групп</h3>
                <div class="d-flex justify-content-end">
                    <a href="{{route('admin.groups.create')}}" class="ordinary-button orange">
                    Создать группу<i class="far fa-plus-square"></i></a>
                </div>
                <div id="group-list" class="items-list">
                    <div class="items-list-header">
                        <div>Название группы</div>
                        <div>Численность</div>
                        <div>Действия</div>
                    </div>
                    <div class="items-list-body">
                        @forelse($groups as $group)
                            <div class="items-list-row">
                                <div class="group-list-row"><a href="{{route('admin.groups.show', $group)}}" title="Перейти к списку детей группы">{{$group->name}}</a></div>
                                <div>{{$group->size}}</div>
                                <form class="actions" onsubmit="if(confirm('Вы действительно хотите удалить?'))
                                   { return true }
                                   else { return false}"
                                      action="{{route('admin.groups.destroy', $group)}}" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}

                                    <a title="Изменить элемент" href="{{route('admin.groups.edit', $group)}}" class="ordinary-button green"><i class="far fa-edit"></i></a>
                                        <button type="submit" title="Удалить элемент" class="ordinary-button orange"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </div>
                        @empty
                            <div class="items-list-row">
                                <div class="mid_text">Данные отсутствуют</div>
                                <div> :( </div>
                                <div> :( </div>
                            </div>
                        @endforelse
                    </div>
                </div>
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        {{$groups->links()}}
                    </ul>
                </nav>
            </div>
        </div>
    </main>

@endsection