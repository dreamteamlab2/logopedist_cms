<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Главная</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.groups.index')}}">Список групп</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Список детей группы "{{$group->name}}"</li>
                    </ol>
                </nav>

                <h3>Список детей группы "{{$group->name}}"</h3>
                <div class="d-flex justify-content-end">
                    <a href="{{route('admin.groups.edit', $group)}}" class="ordinary-button orange">
                    Изменить список<i class="far fa-plus-square"></i></a>
                </div>
                <div id="user-children-list" class="people-list">
                    <div class="people-list-header">ФИО ребенка</div>
                    <div class="people-list-body">
                        @forelse($students as $student)
                            <div class="people-list-row"><a href="{{route('admin.user.show', $student->parent)}}" title="Перейти к персональной странице родителя">{{ $student->name }}</a></div>
                        @empty
                            <div class="people-list-row mid_text">Данные отсутствуют</div>
                        @endforelse
                    </div>
                </div>
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        {{$students->links()}}
                    </ul>
                </nav>
            </div>
        </div>
    </main>

@endsection