{{--форма для создания и обновления--}}
<div class="form-group">
    <label for="group-name">Название группы:</label>
    <input id="group-name" type="text" class="form-control" name="group-name"
           placeholder="{{$group->name ?? "Название группы"}}" required>
</div>
<h6>Выберите детей из списка ниже:</h6>
<div id="choose-child-list" class="form-group">
    @if($students != null)
        @foreach($students as $student)
            <div class="checkbox">
                <input id="child_{{$student->child_id}}" type="checkbox" name="selected[]" value="{{$student->child_id}}"
                       checked>
                <label for="child_{{$student->child_id}}">{{$student->name}}</label>
            </div>
        @endforeach
    @endif

    @forelse($available as $student)
        <div class="checkbox">
            <input id="child_{{$student->child_id}}" type="checkbox" name="selected[]" value="{{$student->child_id}}">
            <label for="child_{{$student->child_id}}">{{$student->name}}</label>
        </div>
    @empty
        <option>Нет доступных для добавления в группу учеников!</option>
    @endforelse
</div>
<div class="d-flex justify-content-end">
    <a href="{{route('admin.groups.index')}}" class="ordinary-button grey" type="button"
       style="margin-right: 10px"> Отменить</a>
    <button class="ordinary-button {{(count($available) == 0) ? "disabled" : "orange"}}"
            type="submit" {{(count($available) == 0) ? "disabled" : ""}}>Сохранить
    </button>
</div>