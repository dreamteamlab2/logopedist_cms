@extends('layouts.app')
@include('admin.layouts.app_admin')

@section('content')
    <main class="l-main">
        <div class="content-wrapper" id="user-profile">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Главная</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.user.index')}}">Список пользователей</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Личная карточка пользователя</li>
                    </ol>
                </nav>
                <div class="row">
                    <div class="user-avatar col-lg-2 col-md-3">
                        <img src="{{ asset('img/avatar.png') }}" class="img-responsive">
                    </div>
                    <div class="col-lg-10 col-md-9 col-sm-6 col-12 user-info">
                        <span class="name">{{$user->name}}</span>
                        <div class="user-info-group">
                            <label><i class="fas fa-mobile-alt"></i>Телефон:</label>
                            <span>{{$user->phone}}</span>
                        </div>
                        <div class="user-info-group">
                            <label><i class="far fa-envelope"></i>E-mail:</label>
                            <span>{{$user->email}}</span>
                        </div>
                    </div>
                </div>
                <div class="children _margin-top-30">
                    <span class="main-title">Дети</span>
                    @forelse($user->children as $child)
                        <div class="child d-flex justify-content-between">
                            <div class="children-info">
                                <div class="children-info-group">
                                    <label class="">Имя: </label>
                                    <span class="">{{$child->name}}</span>
                                </div>
                                <div class="children-info-group">
                                    <label class="">Дата рождения:</label>
                                    <span class="">{{date('d-m-Y',strtotime($child->birthdate))}}</span>
                                </div>
                                <div class="children-info-group">
                                    <label class="">Карта личности: </label>
                                    <span class=""><a href="#">Скачать файл</a></span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <button class="ordinary-button orange" data-toggle="modal"
                                        data-target="#personalCardModal" data-whatever="@mdo">Загрузить карту личности
                                </button>
                            </div>
                        </div>
                    @empty
                        <h5>Этот пользователь еще не добавил информацию о детях</h5>
                    @endforelse
                </div>
            </div>
        </div>
        <!--Модальное окно для добавления карты личности ребенка -->
        <div class="modal fade" id="personalCardModal" tabindex="-1" role="dialog"
             aria-labelledby="personalCardModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="personalCardModalLabel">Добавление карты личности ребенка</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <span class="required_star">*</span>
                                <label for="file-card" class="col-form-label">Файл :</label>
                                <input type="file" class="" id="file-card" required>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
