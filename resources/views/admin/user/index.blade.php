@extends('layouts.app')
@include('admin.layouts.app_admin')

@section('content')
    <main class="l-main">
        <div class="content-wrapper">
            <div id="user-children-list" class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Пользователи @endslot
                @endcomponent
                <h3>Родители</h3>
                <div class="d-flex justify-content-between align-items-center">
                    <div id="search_area" class="d-flex">
                        <input class="search_field" type="text" placeholder="Текст для поиска...">
                    </div>
                    <div>
                        <a href="{{route('admin.user.index')}}" title="Переключиться на список родителей" class="ordinary-button green">Родители</a>
                        <a href="{{route('admin.children')}}" title="Переключиться на список детей" class="ordinary-button disabled">Дети</a>
                    </div>
                </div>

                <div class="people-list">
                    <div class="people-list-header">ФИО родителя</div>
                    <div class="people-list-body">
                        @forelse($users as $user)
                            <div class="people-list-row"><a href="{{route('admin.user.show', $user)}}" title="Перейти к персональной странице родителя">{{ $user->name }}</a></div>
                        @empty
                            <div class="people-list-row mid_text">Данные отсутствуют</div>
                        @endforelse
                    </div>
                </div>
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        {{$users->links()}}
                    </ul>
                </nav>
            </div>
        </div>
    </main>
@endsection
