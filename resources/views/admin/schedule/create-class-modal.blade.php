<div class="modal fade" id="addEventModal" tabindex="-1" role="dialog" aria-labelledby="addEventModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 550px">
            <div class="modal-header">
                <h4 class="modal-title" id="callModalLabel" style="text-align:center"> Добавить занятие</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form style="padding: 10px">
                    <div class="form-group row">
                        <label for="selectEventType" class="col-sm-4 col-form-label">Тип занятия</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="selectEventType">
                                <option hidden disabled selected>Выберите тип занятия</option>
                                <option>
                                    <a data-toggle="collapse" href="#collapseChildName" role="button"
                                       aria-expanded="false" aria-controls="collapseChildName">Групповое</a>
                                </option>
                                <option>
                                    <a data-toggle="collapse" href="#collapseGroupName" role="button"
                                        aria-expanded="false" aria-controls="collapseGroupName">Индивидуальное</a>
                                </option>
                            </select>
                        </div>
                    </div>

                    <!--Скрыты пока не будет выбран элемент из списка-->
                    <div class="collapse" id="collapseChildName">
                        <div class="form-group row">
                            <label  class="col-sm-4 col-form-label">Имя ребенка</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control"required>
                            </div>
                        </div>
                    </div>
                    <div class="collapse" id="collapseGroupName">
                        <div class="form-group row">
                            <label  class="col-sm-4 col-form-label">Название группы</label>
                            <div class="col-sm-8">
                                <input name="" type="text" class="form-control"required>
                            </div>
                        </div>
                    </div>
                    <!-- -------------------- -------------------------- -->
                    <div class="form-group row">
                        <label  class="col-sm-4 col-form-label">Дата занятия</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" id="date" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-4 col-form-label">Время начала</label>
                        <div class="col-sm-8">
                            <input type="time" class="form-control" id="start-time" name="start-time" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Время окончания</label>
                        <div class="col-sm-8">
                            <input type="time" class="form-control" id="end-time" name="end-time"  required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="ordinary-button grey" data-dismiss="modal">Отмена</button>
                <button type="button" class="ordinary-button orange">Добавить</button>
            </div>
        </div>
    </div>
</div>
