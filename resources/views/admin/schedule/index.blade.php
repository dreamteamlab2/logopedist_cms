<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')
<!-- А весь контент содержать в блоке "content" -->
@section('content')
    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="">
                <div class="d-flex justify-content-end">
                    <button id="addEventButton" class="ordinary-button orange"
                        title="Нажмите чтобы добавления занятие в расписание" data-toggle="modal" data-target="#addEventModal"
                        data-whatever="@mdo">Добавить занятие<i class="far fa-plus-square"></i></button>
                </div>
                <div class="col-lg-12 container-fluid">
                    <div class="cd-schedule loading">
                        @include('admin.schedule.timeline')
                        @include('admin.schedule.events')
                    </div>
                </div>
            </div>
        </div>
        @include('admin.schedule.create-class-modal')
    </main>




@endsection
