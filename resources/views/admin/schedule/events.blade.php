<div class="events">
    <ul>
        <li class="events-group">
            <div class="top-info"><span>Понедельник</span></div>
            <ul>
                <li class="single-event" data-start="09:30" data-end="10:30" data-event="group-event">
                    <a href="#0"> <em class="event-name">Групповое занятие</em> </a>
                </li>

                <li class="single-event" data-start="11:00" data-end="12:30" data-event="individual-event">
                    <a href="#0"> <em class="event-name">Новоселов Миша</em> </a>
                </li>

                <li class="single-event" data-start="14:00" data-end="15:15" data-event="individual-event">
                    <a href="#0"> <em class="event-name">Краснов Паша</em> </a>
                </li>
            </ul>
        </li>

        <li class="events-group active" style=" background-color: rgba(255, 198, 129, 0.25)">
            <div class="top-info"><span>Вторник</span></div>
            <ul>
                <li class="single-event" data-start="9:30" data-end="11:00" data-event="group-event">
                    <a href="#0"> <em class="event-name">Групповое занятие</em> </a>
                </li>

                <li class="single-event" data-start="11:30" data-end="13:00" data-event="individual-event">
                    <a href="#0"> <em class="event-name">Аня Смирнова</em> </a>
                </li>

                <li class="single-event" data-start="13:30" data-end="15:00"  data-event="individual-event">
                    <a href="#0"> <em class="event-name">Костя Сидоров</em> </a>
                </li>

                <li class="single-event" data-start="15:45" data-end="16:45" data-event="group-event">
                    <a href="#0"> <em class="event-name">Групповое занятие</em> </a>
                </li>
            </ul>
        </li>

        <li class="events-group">
            <div class="top-info"><span>Среда</span></div>
            <ul>
                <li class="single-event" data-start="09:00" data-end="10:15" data-event="group-event">
                    <a href="#0"> <em class="event-name">Групповое занятие</em> </a>
                </li>

                <li class="single-event" data-start="10:45" data-end="11:45" data-event="individual-event">
                    <a href="#0"> <em class="event-name">Новопашина Юля</em> </a>
                </li>

                <li class="single-event" data-start="12:00" data-end="13:45" data-event="group-event">
                    <a href="#0"> <em class="event-name">Групповое занятие</em> </a>
                </li>
            </ul>
        </li>

        <li class="events-group">
            <div class="top-info"><span>Четверг</span></div>

            <ul>
                <li class="single-event" data-start="09:30" data-end="10:30" data-event="group-event">
                    <a href="#0">
                        <em class="event-name">Групповое занятие</em>
                    </a>
                </li>

                <li class="single-event" data-start="12:00" data-end="13:45" data-event="individual-event">
                    <a href="#0">
                        <em class="event-name">Смирнов Дима</em>
                    </a>
                </li>

                <li class="single-event" data-start="15:30" data-end="16:30" data-event="individual-event">
                    <a href="#0">
                        <em class="event-name">Антонова Ксюша</em>
                    </a>
                </li>
            </ul>
        </li>

        <li class="events-group">
            <div class="top-info"><span>Пятница</span></div>

            <ul>
                <li class="single-event" data-start="12:30" data-end="14:00" data-event="individual-event">
                    <a href="#0">
                        <em class="event-name">Волкова Аня</em>
                    </a>
                </li>

                <li class="single-event" data-start="15:45" data-end="16:45" data-event="individual-event">
                    <a href="#0">
                        <em class="event-name">Никитин Антон</em>
                    </a>
                </li>
            </ul>
        </li>

        <li class="events-group">
            <div class="top-info"><span>Суббота</span></div>
            <ul>
                <li class="single-event" data-start="10:00" data-end="11:00" data-event="individual-event">
                    <a href="#0">
                        <em class="event-name">Морозов Кирилл</em>
                    </a>
                </li>
            </ul>
        </li>

        <li class="events-group">
            <div class="top-info"><span>Воскресенье</span></div>
            <ul>
                <li class="single-event" data-start="10:00" data-end="11:00" data-event="group-event">
                    <a href="#0">
                        <em class="event-name">Групповое занятие</em>
                    </a>
                </li>

                <li class="single-event" data-start="12:30" data-end="14:00" data-event="individual-event">
                    <a href="#0">
                        <em class="event-name">Гришина Настя</em>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
