{{--форма для создания и обновления--}}
<div class="form-group">
    <label for="title">Заголовок</label>
    <input id="title" type="text" class="form-control" name="title"
           placeholder="Заголовок категории" value="{{$direction->title ?? ''}}" required>
</div>
<div class="form-group">
    <label for="description">Описание</label>
    <textarea id="description" type="text" class="form-control" name="description" placeholder="Описание достижения" required>
            {{$direction->description ?? ""}}
    </textarea>
</div>
<div class="form-group">
    <label for="image">Изображение</label>
    <input id="image" type="file" name="image" value="{{$direction->image ?? ""}}" {{$image_required ?? ""}}>
</div>
{{--Вывод картинки--}}
@if($direction->image)
    <img src="{{asset('/storage/directions/' . $direction->image)}}" alt="{{$direction->image ?? ""}}">
@endif

<div class="d-flex justify-content-end">
    <a href="{{route('admin.direction.index')}}" class="ordinary-button grey" type="button" style="margin-right: 10px"> Отменить</a>
    <button class="ordinary-button orange" type="submit">Сохранить</button>
</div>

    {{--<div class="col-lg-6">--}}
        {{--<label for="">Заголовок</label>--}}
        {{--<input type="text" class="form-control" name="title"--}}
               {{--placeholder="Заголовок категории" value="{{$direction->title ?? ''}}" required>--}}

        {{--<label for="">Описание</label>--}}
        {{--<textarea type="text" class="form-control" name="description"--}}
               {{--placeholder="Заголовок категории" required>--}}
            {{--{{$direction->description ?? ''}}--}}
        {{--</textarea>--}}

        {{--<label for="">Изображение</label>--}}
        {{--<input type="file" name="image" value="{{$direction->image ?? ""}}" style="display: block">--}}

        {{--<a href="{{route('admin.description.index')}}" class="btn" type="button"> Отменить</a>--}}
        {{--<input class="btn btn-primary" type="submit" value="Сохранить" style="margin-top: 20px;">--}}
    {{--</div>--}}

