<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Направления работы @endslot
                @endcomponent

                <h3>Направления работы</h3>
                <div class="d-flex justify-content-end">
                    <a href="{{route('admin.direction.create')}}" class="ordinary-button orange">
                        Добавить<i class="far fa-plus-square"></i></a>
                </div>
                    <div id="directions-list" class="items-list">
                        <div class="items-list-header">
                            <div>Картинка</div>
                            <div>Заголовок</div>
                            <div>Описание</div>
                            <div>Действия</div>
                        </div>
                        <div class="items-list-body">
                            @forelse($directions as $direction)
                                <div class="items-list-row">
                                    <div>
                                        <img style="width: 50px;" src="{{asset('/storage/directions/previews/' . $direction->image)}}" alt="{{$direction->image ?? ""}}">
                                    </div>
                                    <div>{{$direction->title}}</div>
                                    <div>{{$direction->description}}</div>
                                    <form class="actions" onsubmit="if(confirm('Вы действительно хотите удалить?'))
                                   { return true }
                                   else { return false}"
                                          action="{{route('admin.direction.destroy', $direction)}}" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                        {{ csrf_field() }}

                                        <a title="Изменить элемент" href="{{route('admin.direction.edit', $direction)}}" class="ordinary-button green"><i class="far fa-edit"></i></a>
                                        <button type="submit" title="Удалить элемент" class="ordinary-button orange"><i class="far fa-trash-alt"></i></button>
                                    </form>
                                </div>
                            @empty
                                <div class="items-list-row">
                                    <div> :( </div>
                                    <div> :( </div>
                                    <div class="mid_text">Данные отсутствуют</div>
                                    <div> :( </div>
                                </div>
                            @endforelse
                        </div>
                    </div>
                    <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-center">
                            {{$directions->links()}}
                        </ul>
                    </nav>

                  {{--<table class="table table-striped">--}}
                    {{--<tbody>--}}
                        {{--@forelse($directions as $direction)--}}
                            {{--<tr>--}}
                                {{--обращение к коллекции, а не к массиву, поэтому без квадрат скобок--}}
                                {{--<td>{{$direction->title}}</td>--}}
                                {{--<td>{{$direction->description}}</td>--}}
                                {{--<td class="text-right">--}}
                                    {{--<form onsubmit="if(confirm('Удалить?')){return true} else {return false}"--}}
                                          {{--action="{{route('admin.direction.destroy', ['id'=>$direction])}}" method="post">--}}
                                        {{--<input type="hidden" name="_method" value="DELETE">--}}
                                        {{--{{ csrf_field() }}--}}
                                        {{--<a class="btn btn-default" href="{{route('admin.direction.edit', $direction)}}"> <i class="fa fa-edit"></i></a>--}}
                                        {{--<button type="submit" class="btn"><i class="far fa-trash-alt"></i></button>--}}
                                    {{--</form>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--@empty--}}
                            {{--<tr>--}}
                                {{--<td colspan="3" class="text-center">--}}
                                    {{--<h2>Данные отсутствуют</h2>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--@endforelse--}}
                    {{--</tbody>--}}
                    {{--<tfoot>--}}
                        {{--<tr>--}}
                            {{--<td colspan="4">--}}
                                {{--<ul class="pagination pull-right">--}}
                                    {{--отрисовка постраничного перехода--}}
                                    {{--links - тоже хелпер--}}
                                    {{--{{$directions->links()}}--}}
                                {{--</ul>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                    {{--</tfoot>--}}
                {{--</table>--}}
            </div>
        </div>
    </main>
@endsection
