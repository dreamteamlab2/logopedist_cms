<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Новости @endslot
                @endcomponent

                <div class="large_text">Редактирование новости</div>
                <form class="form-horizontal" action="{{route('admin.news.update', $news_item)}}" method="post"
                      enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put">
                    {{--передаем токен в форме через хелпер--}}
                    {{ csrf_field() }}

                    {{--Form Include--}}
                    @include('admin.news.form',  ['news_item' => $news_item, 'image-required' => ''])
                </form>
            </div>
        </div>
    </main>

@endsection