<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
      <div class="content-wrapper">
          <div class="container">
              @component('admin.components.breadcrumbs')
                  @slot('active') Новости @endslot
              @endcomponent
              <h3>Новости</h3>
              <div class="d-flex justify-content-end">
                  <a href="{{route('admin.news.create')}}" class="ordinary-button orange">
                    Добавить<i class="far fa-plus-square"></i></a>
              </div>
                  <div id="news-list" class="items-list">
                      <div class="items-list-header">
                          <div>Дата</div>
                          <div>Заголовок</div>
                          <div>Текст</div>
                          <div>Действия</div>
                      </div>
                      <div class="items-list-body">
                          @forelse($news as $news_item)
                              <div class="items-list-row">
                                  <div>
                                      {{--тут можно не дату создания, а дату последнего изменения (она тож в базе лежит)--}}
                                      {{date('d-m-Y',strtotime($news_item->created_at))}}
                                      {{--<img style="width: 50px;" src="{{asset('/storage/achievements/previews/' . $achievement->image)}}" alt="{{$achievement->image ?? ""}}">--}}
                                  </div>
                                  <div>{{$news_item->title}}</div>
                                  <div>{{$news_item->description}}</div>
                                  <form class="actions" onsubmit="if(confirm('Вы действительно хотите удалить?'))
                                   { return true }
                                   else { return false}"
                                        action="{{route('admin.news.destroy', $news_item)}}" method="post">
                                      <input type="hidden" name="_method" value="DELETE">
                                      {{ csrf_field() }}

                                      <a title="Изменить элемент" href="{{route('admin.news.edit', $news_item)}}" class="ordinary-button green"><i class="far fa-edit"></i></a>
                                      <button type="submit" title="Удалить элемент" class="ordinary-button orange"><i class="far fa-trash-alt"></i></button>
                                  </form>
                              </div>
                          @empty
                              <div class="items-list-row">
                                  <div> :( </div>
                                  <div> :( </div>
                                  <div class="mid_text">Данные отсутствуют</div>
                                  <div> :( </div>
                              </div>
                          @endforelse
                      </div>
                  </div>
                  <nav aria-label="Page navigation">
                      <ul class="pagination justify-content-center">
                          {{$news->links()}}
                      </ul>
                  </nav>
              {{--таблица списка--}}
              {{--<table class="table table-striped">--}}
                {{--<tbody>--}}
                    {{--@forelse($news as $news_item)--}}
                        {{--<tr>--}}
                            {{--тут можно не дату создания, а дату последнего изменения (она тож в базе лежит)--}}
                            {{--<td>{{date('d-m-Y',strtotime($news_item->created_at))}}</td>--}}
                            {{--<td>{{$news_item->title}}</td>--}}
                            {{--<td>{{$news_item->description}}</td>--}}
                            {{--<td class="text-right">--}}
                                {{--<form onsubmit="if(confirm('Удалить?')){return true} else {return false}"--}}
                                      {{--action="{{route('admin.news.destroy', ['id'=>$news_item])}}" method="post">--}}
                                    {{--<input type="hidden" name="_method" value="DELETE">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--<a class="btn btn-default" href="{{route('admin.news.edit', $news_item)}}">--}}
                                        {{--<i class="fa fa-edit"></i>--}}
                                    {{--</a>--}}
                                    {{--<button type="submit" class="btn"><i class="far fa-trash-alt"></i></button>--}}
                                {{--</form>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                    {{--@empty--}}
                        {{--<tr>--}}
                            {{--<td colspan="4" class="text-center">--}}
                                {{--<h2>Данные отсутствуют</h2>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                    {{--@endforelse--}}
                {{--</tbody>--}}
                {{--<tfoot>--}}
                    {{--<tr>--}}
                        {{--<td colspan="4">--}}
                            {{--<ul class="pagination pull-right">--}}
                                {{--отрисовка постраничного перехода--}}
                                {{--links - тоже хелпер--}}
                                {{--{{$news->links()}}--}}
                            {{--</ul>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--</tfoot>--}}
            {{--</table>--}}
          </div>
      </div>
    </main>


@endsection