{{--форма для создания и обновления--}}
<div class="form-group">
    <label for="title">Заголовок</label>
    <input id="title" type="text" class="form-control" name="title"
           placeholder="Заголовок категории" value="{{$news_item->title ?? ''}}" required>
</div>
<div class="form-group">
    <label for="description">Текст новости</label>
    <textarea id="description" type="text" class="form-control" name="description" placeholder="Описание достижения" required>
            {{$news_item->description ?? ""}}
    </textarea>
</div>
<div class="form-group">
    <label for="short-description">Краткое описание</label>
    <textarea type="text" class="form-control" name="short_description" id="short-description"
              placeholder="Краткое описание" required>
            {{$news_item->short_description ?? ''}}
        </textarea>
</div>
<div class="form-group">
    <label for="image">Изображение</label>
    <input id="image" type="file" name="image" value="{{$news_item->image ?? ""}}" {{$image_required ?? ""}}>
</div>
{{--Вывод картинки--}}
@if($news_item->image)
    <img src="{{asset('/storage/news/' . $news_item->image)}}" alt="{{$news_item->image ?? ""}}">
@endif

<div class="d-flex justify-content-end">
    <a href="{{route('admin.news.index')}}" class="ordinary-button grey" type="button" style="margin-right: 10px"> Отменить</a>
    <button class="ordinary-button orange" type="submit">Сохранить</button>
</div>

{{--<div class="container">--}}
    {{--<div class="col-lg-6">--}}
        {{--<label for="">Заголовок</label>--}}
        {{--<input type="text" class="form-control" name="title"--}}
               {{--placeholder="Заголовок категории" value="{{$news_item->title ?? ''}}" required>--}}

        {{--<label for="">Краткое описание</label>--}}
        {{--<textarea type="text" class="form-control" name="short_description"--}}
                  {{--placeholder="Краткое описание" required>--}}
            {{--{{$news_item->short_description ?? ''}}--}}
        {{--</textarea>--}}

        {{--<label for="">Текст новости</label>--}}
        {{--<textarea type="text" class="form-control" name="description"--}}
               {{--placeholder="Текст новости" required>--}}
            {{--{{$news_item->description ?? ''}}--}}
        {{--</textarea>--}}

        {{--<label for="">Изображение</label>--}}
        {{--<input type="file" name="image" value="{{$news_item->image ?? ""}}" style="display: block">--}}

        {{--Кнопка отмены выглядит крайне стремно, но пока что она тут просто, чтобы было :) --}}
        {{--<a href="{{route('admin.news.index')}}" class="btn" type="button"> Отменить</a>--}}
        {{--<input class="btn btn-primary" type="submit" value="Сохранить" style="margin-top: 20px;">--}}
    {{--</div>--}}
{{--</div>--}}
