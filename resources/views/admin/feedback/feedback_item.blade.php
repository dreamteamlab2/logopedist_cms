<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Заявки @endslot
                @endcomponent

                @if ($feedback->type === "phone")
                    <div class="main-title"> Заявка на звонок</div>
                @else
                    <div class="main-title"> Вопрос</div>
                @endif
                <form class="info-form">
                    <div class="form-group row">
                      <label class="col-md-2 col-form-label"><i class="far fa-calendar-alt"></i> Дата </label>
                      <div class="col-md-10 col-form-label">{{date('d-m-Y',strtotime($feedback->created_at))}}</div>
                  </div>
                  <div class="form-group row">
                      <label class="col-md-2 col-form-label"><i class="far fa-user"></i> Отправитель </label>
                      <div class="col-md-10 col-form-label">{{$feedback->sender}}</div>
                  </div>
                  <div class="form-group row">
                      <label class="col-md-2 col-form-label"><i class="fas fa-mobile-alt"></i>Телефон: </label>
                      <div class="col-md-10 col-form-label">{{$feedback->phone}}</div>
                  </div>
                    @isset($feedback->email)
                        <div class="form-group row">
                          <label class="col-md-2 col-form-label"><i class="far fa-envelope"></i>E-mail: </label>
                          <div class="col-md-10 col-form-label">{{$feedback->email}}</div>
                      </div>
                    @endisset

                    @isset($feedback->message)
                        <div class="form-group row">
                          <label class="col-md-2 col-form-label"><i class="fas fa-envelope-open-text"></i>Сообщение </label>
                          <div class="col-md-10 col-form-label">{{$feedback->message}}</div>
                      </div>
                    @endisset
                    @if ($feedback->type === "phone")
                        <div class="form-group row">
                            <label class="col-md-12 col-form-label"><i class="far fa-check-circle"></i>
                                Заявка была отправлена через форму "Заказать обратный звонок"
                            </label>
                        </div>
                    @else
                        <div class="form-group row">
                            <label class="col-md-12 col-form-label"><i class="far fa-check-circle"></i>
                                Заявка была отправлена через форму "Задать вопрос"
                            </label>
                        </div>
                    @endif
              </form>
            </div>
        </div>
    </main>
@endsection
