<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Заявки @endslot
                @endcomponent
                <h3>Заявки</h3>
                    <div id="" class="items-list">
                        <div class="items-list-header" style="grid-template-columns:1fr 3fr 4fr 1fr;!important;">
                            <div>Дата</div>
                            <div>Отправитель</div>
                            <div>Текст</div>
                            <div>Действие</div>
                        </div>
                        <div class="items-list-body">
                            @forelse($feedbacks as $feedback)
                                <div class="items-list-row"  style="grid-template-columns:1fr 3fr 4fr 1fr;!important;">
                                    <div>{{date('d-m-Y',strtotime($feedback->created_at))}}</div>
                                    <div>
                                        <a href="{{route('admin.feedback.show', $feedback)}}">{{$feedback->sender}}</a>
                                    </div>
                                    <div>{{$feedback->message}}</div>

                                     <form class="actions" onsubmit="if(confirm('Вы действительно хотите удалить?'))
                                       { return true }
                                       else { return false}"
                                               action="{{route('admin.feedback.destroy', $feedback)}}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                             {{ csrf_field() }}
                                             <button type="submit" title="Удалить элемент" class="ordinary-button orange"><i class="far fa-trash-alt"></i></button>
                                    </form>
                                </div>
                            @empty
                                <div class="items-list-row">
                                    <div> :( </div>
                                    <div> :( </div>
                                    <div class="mid_text">Данные отсутствуют</div>
                                    <div> :( </div>
                                </div>
                            @endforelse
                        </div>
                    </div>
                    <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-center">
                            {{$feedbacks->links()}}
                        </ul>
                    </nav>
            </div>
        </div>
    </main>
@endsection
