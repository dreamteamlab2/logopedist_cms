<h3>Основная информация</h3>

{{--@forelse($info as $info_item)--}}
    <div class="d-flex justify-content-end" style="margin-bottom: 20px">
        <a href="{{route('admin.dashboard.edit')}}" class="ordinary-button orange">
            Редактировать<i class="fa fa-edit"></i>
        </a>
    </div>

    <table class="table table-striped table-bordered table-sm" style="margin-left: 20px;line-height: 30px">
        <tr class="">
            <td class="">Адрес</td>
            {{--<td class="">{{$info_item->address}}</td>--}}
            <td class="">Томск, пл. Кирова 58, стр. 55, оф. 69</td>
        </tr>
        <tr class="">
            <td class="">Телефон</td>
            <td class="">+7-903-952-98-18</td>
        </tr>
        <tr class="">
            <td class="">Электронная почта</td>
            <td class="">t_kajer@mail.ru</td>
        </tr>
        <tr class="">
            <td class="">Информация о логопеде</td>
            {{--<td class="">{{$info_item->logopedist_description}}</td>--}}
            <td class="">Логопед первой квалификационной категории. В 2001 году окончила Московский государственный открытый педагогический университет им. М.А. Шолохова по специальности «Логопедия». Стаж педагогической деятельности 13 лет.</td>
        </tr>
        <tr class="">
            <td class="">Информация о сайте</td>
            {{--<td class="">{{$info_item->site_description}}</td>--}}
            <td class="">Сайт предназначен для детской образовательной среды в городе Томске</td>
        </tr>
    </table>
{{--@empty--}}
    {{--<h4>Данные отсутствуют</h4>--}}
{{--@endforelse--}}

