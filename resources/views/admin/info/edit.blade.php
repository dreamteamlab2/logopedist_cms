{{--указываем макет--}}
@extends('layouts.app')
@include('admin.layouts.app_admin')

{{--секция с контентом--}}
@section('content')
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Информация о сайте @endslot
                @endcomponent

                <div class="col-lg-6">
                    <h4>Редактирование информации</h4>
                    <form class="form-horizontal" action="{{route('admin.dashboard.update', $info_item)}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="put">
                        {{--передаем токен в форме через хелпер--}}
                        {{ csrf_field() }}

                        {{--Form Include--}}
                        {{--@include('admin.info.form')--}}

                        <label for="">Информация о логопеде</label>
                        {{--<input type="text" class="form-control" name="logopedist_description"--}}
                        {{--value="{{$info_item->logopedist_description ?? ''}}" required>--}}

                        <textarea type="text" class="form-control" name="logopedist_description" required>
                            {{$info_item->logopedist_description ?? ''}}
                        </textarea>

                        <label for="">Информация о сайте</label>
                        <input type="text" class="form-control" name="site_description"
                               value="{{$info_item->site_description ?? ''}}" required>

                        <label for="">Адрес</label>
                        <input type="text" class="form-control" name="address"
                               value="{{$info_item->address ?? ""}}" style="display: block" required>

                        <label for="">Телефон</label>
                        <input type="text" class="form-control" name="phone"
                               value="" style="display: block">

                        <label for="">Электронная почта</label>
                        <input type="text" class="form-control" name="email"
                               value="" style="display: block">

                        <input class="btn btn-primary" type="submit"
                               value="Сохранить" style="margin-top: 20px;">
                    </form>
                </div>
            </div>
        </div>
    </main>


@endsection