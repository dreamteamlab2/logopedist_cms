<div class="sidebar-is-reduced">

    <header class="l-header" style="height: 50px">
        <div class="l-header__inner clearfix">

            <div class="c-header-icon js-hamburger">
                <div class="hamburger-toggle">
                    <span class="bar-top"></span>
                    <span class="bar-mid"></span>
                    <span class="bar-bot"></span>
                </div>
            </div>
            <div class="c-header-icon">
                <span class="c-badge c-badge--header-icon">7</span><i class="fa fa-bell"></i>
            </div>
            {{--Logout--}}
            <div class="admin_name">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf
                        </form>
                    </div>
                @endguest
            </div>
        </div>
    </header>
    <div class="l-sidebar">
        <div class="logo">
            <div class="logo__txt"></div>
        </div>
        <div class="l-sidebar__content">
            <nav class="c-menu js-menu">
                <ul class="u-list">
                    <li class="c-menu__item is-active" data-toggle="tooltip" title="Расписание">
                        <a href="">
                            <div class="c-menu__item__inner"><i class="fas fa-calendar-alt"></i>
                                <div class="c-menu-item__title"><span>Расписание</span></div>
                            </div>
                        </a>
                    </li>
                    <li class="c-menu__item" data-toggle="tooltip" title="Новости">
                        <a href="{{route('admin.news.index')}}">
                            <div class="c-menu__item__inner"><i class="far fa-file-alt"></i>
                                <div class="c-menu-item__title"><span>Новости</span></div>
                            </div>
                        </a>
                    </li>
                    <li class="c-menu__item" data-toggle="tooltip" title="Отзывы">
                        <div class="c-menu__item__inner"><i class="far fa-comments"></i>
                            <div class="c-menu-item__title"><span>Отзывы</span></div>
                        </div>
                    </li>
                    <li class="c-menu__item" data-toggle="tooltip" title="Заявки">
                        <a href="{{route('admin.feedback.index')}}">
                            <div class="c-menu__item__inner"><i class="far fa-bell"></i>
                                <div class="c-menu-item__title"><span>Заявки</span></div>
                            </div>
                        </a>
                    </li>
                    <li class="c-menu__item" data-toggle="tooltip" title="Пользователи">
                        <a href="{{route('admin.user.index')}}">
                            <div class="c-menu__item__inner"><i class="far fa-user"></i>
                                <div class="c-menu-item__title"><span>Пользователи</span></div>
                            </div>
                        </a>
                    </li>
                    <li class="c-menu__item" data-toggle="tooltip" title="Ученики">
                        <a href="{{route('admin.children')}}">
                            <div class="c-menu__item__inner"><i class="far fa-laugh-squint"></i>
                                <div class="c-menu-item__title"><span>Ученики</span></div>
                            </div>
                        </a>
                    </li>
                    <li class="c-menu__item" data-toggle="tooltip" title="Группы">
                        <a href="{{route('admin.groups.index')}}">
                            <div class="c-menu__item__inner"><i class="far fa-list-alt"></i>
                                <div class="c-menu-item__title"><span>Группы</span></div>
                            </div>
                        </a>
                    </li>
                    <li class="c-menu__item" data-toggle="tooltip" title="Достижения">
                        <a href="{{route('admin.achievements.index')}}">
                            <div class="c-menu__item__inner">
                                <i class="far fa-star"></i>
                                <div class="c-menu-item__title"><span>Достижения</span></div>
                            </div>
                        </a>
                    </li>
                    <li class="c-menu__item" data-toggle="tooltip" title="Направления">
                        <a href="{{route('admin.direction.index')}}">
                            <div class="c-menu__item__inner"><i class="far fa-paper-plane"></i>
                                <div class="c-menu-item__title"><span>Направления</span></div>
                            </div>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
