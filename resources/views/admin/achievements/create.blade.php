<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Достижения @endslot
                @endcomponent

                <div class="large_text">Добавление нового достижения</div>
                <form class="form-horizontal" action="{{route('admin.achievements.store')}}" method="post" enctype="multipart/form-data">
                    {{--передаем токен в форме через хелпер--}}
                    {{ csrf_field() }}

                    {{--Form Include--}}
                    @include('admin.achievements.form', ['image_required' => ''])
                </form>
            </div>
        </div>
    </main>
@endsection