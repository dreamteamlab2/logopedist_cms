<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')

    <!-- Вставляем админскую шапку и сайдбар -->
    @include('admin.layouts.sidebar')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <main class="l-main">
        <div class="content-wrapper">
            <div class="container">
                @component('admin.components.breadcrumbs')
                    @slot('active') Достижения @endslot
                @endcomponent

                <h3>Список достижений</h3>
                <div class="d-flex justify-content-end">
                    <a href="{{route('admin.achievements.create')}}" class="ordinary-button orange">
                    Добавить<i class="far fa-plus-square"></i></a>
                </div>
                <div id="achievements-list" class="items-list">
                    <div class="items-list-header">
                        <div>Картинка</div>
                        <div>Описание</div>
                        <div>Действия</div>
                    </div>
                    <div class="items-list-body">
                        @forelse($achievements as $achievement)
                            <div class="items-list-row">
                                <div>
                                    <img style="width: 50px;" src="{{asset('/storage/achievements/previews/' . $achievement->image)}}" alt="{{$achievement->image ?? ""}}">
                                </div>
                                <div>{{$achievement->description}}</div>
                                <form class="actions" onsubmit="if(confirm('Вы действительно хотите удалить?'))
                                   { return true }
                                   else { return false}"
                                      action="{{route('admin.achievements.destroy', $achievement)}}" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}

                                    <a title="Изменить элемент" href="{{route('admin.achievements.edit', $achievement)}}" class="ordinary-button green"><i class="far fa-edit"></i></a>
                                        <button type="submit" title="Удалить элемент" class="ordinary-button orange"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </div>
                        @empty
                            <div class="items-list-row">
                                <div> :( </div>
                                <div class="mid_text">Данные отсутствуют</div>
                                <div> :( </div>
                            </div>
                        @endforelse
                    </div>
                </div>
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        {{$achievements->links()}}
                    </ul>
                </nav>
            </div>
        </div>
    </main>

@endsection