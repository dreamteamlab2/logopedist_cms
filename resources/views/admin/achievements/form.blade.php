{{--форма для создания и обновления--}}

<div class="form-group">
    <label for="description">Описание</label>
    <textarea id="description" type="text" class="form-control" name="description"
              placeholder="Описание достижения" required>
            {{$achievement->description ?? ""}}
        </textarea>
</div>
<div class="form-group">
    <label for="image">Изображение</label>
    <input id="image" type="file" name="image" value="{{$achievement->image ?? ""}}" {{$image_required ?? ""}}>
</div>
{{--Вывод картинки--}}
@if($achievement->image)
    <img src="{{asset('/storage/achievements/' . $achievement->image)}}" alt="{{$achievement->image ?? ""}}">
@endif

<div class="d-flex justify-content-end">
    <a href="{{route('admin.achievements.index')}}" class="ordinary-button grey" type="button" style="margin-right: 10px"> Отменить</a>
    <button class="ordinary-button orange" type="submit">Сохранить</button>
</div>