<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')
    <!-- Вставляем шапку -->
    @include('layouts/header')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-12">
                <!-- Информация о логопеде + фото -->
                <div id="description" class="row">
                    <div class="col-lg-4" style=" text-align: center">
                        <img src="{{ asset('img/index/photo.jpg') }}" style="object-fit: cover; width: 230px; height: 250px;  border-radius: 10px; ">
                    </div>
                    <p class="col-lg-8 description">
                        Логопед первой квалификационной категории. В 2001 году окончила Московский
                        государственный открытый педагогический университет им. М.А.
                        Шолохова по специальности «Логопедия». Стаж педагогической деятельности 13 лет.
                    </p>
                </div>

                <!-- Directions -->
                <div id="directions" class="block">
                    <div class="main-title">Направления работы</div>
                    <div class="row">
                        @forelse($directions as $direction)
                            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                <img src="{{asset('/storage/directions/' . $direction->image)}}" alt="{{$direction->image ?? ""}}">
                                <div class="title">{{$direction->title}}</div>
                                <p class="small_text">{{$direction->description}}</p>
                            </div>
                        @empty
                            <div class="mid_text">Данные отсутствуют</div>
                        @endforelse
                    </div>
                </div>

                <!--Achievements-->
                <div id="achievements" class="block">
                    <div class="main-title">Достижения </div>
                    <div id="achiev-slider">
                        @forelse($achievements as $achievement)
                            <div>
                                <a href="{{asset('/storage/achievements/' . $achievement->image)}}" data-title="{{$achievement->description ?? ""}}" data-lightbox="achievements">
                                    <img src="{{asset('/storage/achievements/' . $achievement->image)}}" alt="{{$achievement->image ?? ""}}">
                                </a>
                            </div>
                        @empty
                            <div class="mid_text">Данные отсутствуют</div>
                        @endforelse
                    </div>
                </div>
            </div>

            <!--Side Bar-->
            <div class="side-bar col-xl-offset-9 col-xl-3 col-lg-12">
                <div class="title"> Новости </div>
                <div class="one_news">
                    <i> Набор в группу от 3 лет </i>
                    <div class="date">25.05.2018</div>
                </div>
                <div class="one_news">
                    <i> Внимание! С 20.03 по 30.03 занятий не будет </i>
                    <div class="date">25.05.2018</div>
                </div>
                <div class="one_news">
                    <i> Набор в группу от 3 лет </i>
                    <div class="date">25.05.2018</div>
                </div>
                <div class="one_news">
                    <i> Набор в группу от 3 лет </i>
                    <div class="date">25.05.2018</div>
                </div>
            </div>
        </div>
    </div>

    <!--Footer-->
    @include('layouts/footer')
    <!--Модальное окно для обратного звонка-->
    @include('layouts/call-modal')

@endsection