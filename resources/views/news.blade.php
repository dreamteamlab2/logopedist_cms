@extends('layouts.app')

@section('content')
    @include('layouts/header')

    <div class="container">
        <div class="main-title">Актуальные новости, будь в курсе
            <i class="far fa-smile-wink"></i>
        </div>
        <div class="row">
            @forelse($news as $news_item)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="one-news card">
                        <div class="title">
                            {{$news_item->title}}
                            <div class="post_date">
                                {{date('d-m-Y',strtotime($news_item->created_at))}}
                            </div>
                        </div>
                        <p> {{$news_item->description}}</p>
                        <a href="#" class="read_more" style="text-align: right">Читать далее</a>
                    </div>
                </div>
            @empty
                <h3>Новостей пока нет, но вы держитесь</h3>
            @endforelse

        </div>
    </div>


    <!--Footer-->
    @include('layouts/footer')
    <!--Модальное окно для обратного звонка-->
    @include('layouts/call-modal')
    <!--Подключение сторонних js-файлов-->
    @include('layouts/js')

@endsection