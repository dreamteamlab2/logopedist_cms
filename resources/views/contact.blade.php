<!-- Все файлы, не являющиеся частями других файлов, должны наследоваться от этого шаблона -->
@extends('layouts.app')

<!-- А весь контент содержать в блоке "content" -->
@section('content')
    <!-- Вставляем шапку -->
    @include('layouts/header')

    <!-- Основное содержание страницы (соответствует названию файла) -->
    <div id="contacts" class="container _margin-top-30">
        <div class="main-title">Контактная информация</div>
        <div class="row">
            <div class="col-lg-6">
                <div class="hint-contacts">
                    <div>
                        <i class="fas fa-mobile-alt green-text-color"></i>
                        <span class=""> Телефон: +7 (903) 952-98-18</span>
                    </div>
                    <div>
                        <i class="far fa-envelope green-text-color"></i>
                        <span class=""> E-mail: t_kajer@mail.ru</span>
                    </div>
                    <div>
                        <i class="fas fa-map-marker-alt green-text-color"></i>
                        <span class="">Адрес: Томск, пл. Кирова 58, стр. 55, оф. 69</span>
                    </div>
                </div>
                <div class="hint-ask">Есть вопросы? Заполните форму, и мы ответим на них
                    <i class="fas fa-arrow-right"></i>
                </div>
            </div>
            <div class="offset-xl-1 col-xl-5 col-lg-6">
                <form class="question-form" action="{{route('admin.feedback.store')}}" method="post">
                    {{ csrf_field() }}
                    <div class="title">Задать вопрос</div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="question_name"  placeholder="Ваше имя" required>
                    </div>
                    <div class="form-group">
                        <input id="question_phone" type="text" class="form-control" placeholder="Номер контактного телефона">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="question_email" placeholder="Электронная почта" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" id="question" cols="30" rows="5" placeholder="Ваш вопрос" required></textarea>
                    </div>
                    <button type="submit" class="ordinary-button orange">Отправить</button>
                </form>
            </div>
        </div>
        <div id="geolocation" class="" >
            <div class="main-title">Местоположение</div>
            <div id="map">
                <!-- Яндекс.Карты -->
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ab8efb11e74bf882952a239235c6aed4b87243c2d63ee2d04e2092f30bb183002&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
    </div>

    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-lg-6">--}}
        {{--<div class="main-title">Контактная информация</div>--}}
        {{--<div class="orange hint main-title col-lg-12">--}}
            {{--<div class="col-lg-3">Телефон:</div><div id="phone" class="col-lg-9">+7 (903) 952-98-18</div>--}}
            {{--<div class="col-lg-3">E-mail:</div><div id="email" class="col-lg-9">t_kajer@mail.ru</div>--}}
            {{--<div class="col-lg-3">Адрес:</div><div id="address" class="col-lg-9">Томск, пл. Кирова 58, стр. 55, оф. 69</div>--}}
        {{--</div>--}}
        {{--<div class="large_text green hint col-lg-8">Есть вопросы? Заполните форму, и мы ответим на них <i class="fas fa-arrow-right"></i></div>--}}
    {{--</div>--}}
    {{--<div class="offset-lg-1 col-lg-5">--}}
        {{--<form class="shadow_form" action="{{route('admin.feedback.store')}}" method="post">--}}
            {{--{{ csrf_field() }}--}}
            {{--<div class="title">Задать вопрос</div>--}}
            {{--<div class="form-group">--}}
                {{--<input name="sender" type="text" class="form-control" id="sender"  placeholder="Ваше имя" required>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<input name="phone" id="phone" type="text" class="form-control" placeholder="Номер контактного телефона">--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<input name="email" type="text" class="form-control" id="email" placeholder="Электронная почта" required>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<textarea class="form-control" name="message" id="message" cols="30" rows="5" placeholder="Ваш вопрос" required></textarea>--}}
            {{--</div>--}}
            {{--<input type="hidden" name="type" value="question">--}}
            {{--<button type="submit" class="btn btn-primary">Отправить </button>--}}
        {{--</form>--}}
    {{--</div>--}}
{{--</div>--}}

    {{--<div id="geolocation" class="block col-lg-12" >--}}
    {{--<div class="main-title">Местоположение</div>--}}
    {{--<div id="map">--}}
        {{--<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A06d1cde049ed3731ba9316f6ed783512784117b5c140505f4260668cd724893e&amp;width=100%25&amp;height=350&amp;lang=ru_RU&amp;scroll=true"></script>--}}
    {{--</div>--}}
{{--</div>--}}
{{--</div>--}}

    <!--Footer-->
    @include('layouts/footer')
    <!--Модальное окно для обратного звонка-->
    @include('layouts/call-modal')

@endsection
